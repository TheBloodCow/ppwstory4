from django.apps import AppConfig


class TempyConfig(AppConfig):
    name = 'Tempy'
